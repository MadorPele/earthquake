const lastPage = 11;
var currentPage;
var currentResponse = 0;
var titles = [
  "מהי רעידת אדמה?",
  "נזקים",
  "רעידות אדמה בישראל",
  "משמעויות מרעידת אדמה",
  "מאמצים",
  "עקרונות המענה",
  'שלבי טיפול באירוע רעא"ד בעורף',
  "מטרת הכוחות השונים",
  'יכולות צה"ל',
  'אחריותנו על פי החלטת הממשלה'
];
var locations = [
  "המוקד באצבע הגליל - בקעת הלבנון",
  "בקעת הירדן",
  "בקעת הלבנון",
  "צפת",
  "צפון ים המלח",
  "צפון ים המלח",
];
var strength = [
  "מגניטודה מעל 7",
  "מגניטודה מעל 6",
  "מגניטודה מעל 6.5",
  "מגניטודה בין 6.5 ל7",
  "מגניטודה בעוצמה של 6.2",
  "מגניטודה בעוצמה של 7.2",
];
var deaths = [
  "מעל ל30 אלף",
  "מאות הרוגים במספר ערים",
  "מעל ל3000",
  "מעל ל7000",
  "מעל ל500",
  "20 הרוגים בנואיבה",
];

var basicConcepts = [
  'זוהי הערכה כמותית לגודלה של רעידת אדמה על בסיס המידה (משרעת, אמפליטודה) של הגלים הסיסמיים ותנודות.',
  'המקום בו החלה רעידת אדמה. נמצא בתת הקרקע על העתק קיים או העתק חדש שנוצר בעת הרעידה. המוקד מכונה גם פוקוס או היפוצנטר הנקודה בפני השטח שמעל המוקד נקראת אפיצנטר.',
  'חוזקה של רעידת אדמה על פי רישומי תנודות הקרקע (הגלים הסיסמיים) וכתלות במרחק מהמוקד. הסולם בנוי בקנה מידה לוגריתמי כאשר עליה ביחידת מגניטודה אחת פירושה הכפלה פי 10 של חוזק הרעידה.'
];

var responseTexts = [
  "ככלל, היערכות לרעידת אדמה מבוססת על מרכיבי מענה גנריים, שפותחו במסגרת המענה לתרחיש המוביל של פיקוד העורף: המלחמה, תוך ביצוע פעולות בניין כוח יעודיות מצומצמות המתאימות לתרחיש רעידת אדמה. ההיערכות והמוכנות הנדרשות בשגרה לקראת מענה לרעידת אדמה כוללות את כלל הרמות החל מרמת הפרט ועד רמת משרדי הממשלה השונים.",
  "בישראל כמיליון מבנים, ומתוכם ע”פ ההערכות הקיימות יש כ -80 אלף מבני מגורים בני 3 קומות ומעלה שלא עומדים בתקן אשר מחייב בנייה עמידה באירוע רעידת אדמה. במטרה לגשר על פער זה, מבוצעים מספר תהליכים הן ברמת המדינה, כגון ת”י 413, תמ”א 38 והן ברמת הפיקוד כגון תכנית מיגון רשתית.",
  "מידע הנדסי, סיסמולוגי, גיאוגרפי וגיאולוגי והמשמעויות הנגזרות, על מנת לאפשר קביעת תרחישים והערכת נזקים מקדימה.",
  "ברמת הארגונים: פיתוח וכתיבת תורות הפעלה, מסמכים מנחים לגופים אזרחיים ותוכניות אופרטיביות ראשיות ומשלימות למעה רעא”ד. הכנת רשימות דרישות אמצעים וציוחד מחו”ל, ברמת הפרט והמשפחה – הכרת הנחיות ההתגוננות, הכנת תוכנית משפחתית וכדומה.",
  "הן ברמת הפרט והן ברמת הארגונים על מנת לייצר מיומנות מרבית. בכלל זה: הגברת מודעות הכנה מנטלית וחילוץ קל.",
];

var responsibilitiesTexts = [
  'בדיקה מתמדת של החוקים התקנות וההנחיות, זיהוי מתקנים אזרחים חיוניים שפגיעה בתפקודם תפגע ברמת מוכנות צה"ל להגן על המדינה.',
  'הכנת תרחישי יחוס יעודיים ופרטניים על בסיס התרחיש הלאומי לכלל יחידות צה"ל, הכשרת חיילי צה"ל להתמודדות אישית, הכנת תכניות פקודות והנחיות ותרגול מצבי החירום בשלבי המענה והשיקום האנושי הראשוני, יצירה של בסיסי נתונים רלוונטים לצה"ל, שילוב הדרכות בנושא התמודדות ברע"ד במסגרת הדרכות המועברות ע"י פקע"ר בנושא הג"א, הערכות למתן מענה וסיוע לוגיסטי לכלל גופי המענה ובכלל זה לקבל את האחריות ממ"י ע"פ פקודה, קשר עם הרש"פ ותיאום העברת סיוע.',
  'סיוע לנפגעי רע"ד ולרשויות מקומיות שנפגעו בכפוף להע"מ, בדיקת תקינות המבנים שבאחריותו, התאמת התכנית לאירוע ולצרכים כפי שעולה מתמונת המצב, פו"ש וטיפול באוכולסיית החיילים באשר הם ומתן מענה לשטחים בהם הוא ריבון, חילוץ והצלת לכודים כולל תעדוף אתרי חילוץ, מתן סיוע לוגיסטי ורפואי והקצאת משאפים ע"פ בקשות לסיוע מרח"ל בכפוף להע"מ (תמ"צ, מערכי שליטה, פינוי, הקמת מחנות, סיוע בהכשרת בתי עלמין ותפעול מחנות עקורים ועוד), גיוס אנשי מילואים, העברת בקשות לסיוע מחו"ל לרח"ל, מתן הנחיות לציבור והפעלת מרכזי מידע לאחר שהאחריות הועברה לצה"ל או כסיוע למ"י, תיאום הרש"פ באמצעות מתאם הפעולות בשטחים.',
  'מיפוי הנזקים במתקנים שבאחריותו, סיוע לנפגעי רע"ד ולרשויות בהתאם להע"מ, הקצאת משאבים (צמ"ה, מיכליות דלק ומים), סיוע בהקמה ותפעול מחנות עקורים, שיקום מבנים ותשתיות של צה"ל, סיוע לוגיסטי, תיאום רש"פ, קליטת משלחות חילוץ מחו"ל.',
  'המשך פעילות שגרתית שבתחום אחריות צה"ל, הכנת תכנית לשיקום יחידות צה"ל, שיקום מבנים ומתקנים שבאחריות צה"ל, תיאום רש"פ.'
];

var responsibilitiesTitles = [
  'מניעה',
  'היערכות ואיפחות',
  'מענה',
  'שיקום אוכלוסין ראשוני',
  'שיקום הקהילה והתשתית הפיזית'
];

var responseTitles = [
  'היערכות מוקדמת לרעא"ד',
  "בניית תשתית מבנים בהתאם לתקן",
  "איסוף מידע, שכבות ונתונים",
  "כתיבת מסמכי מידע",
  "הכשרה, אימון ותרגול",
];

var loadgifs = [
  "media/gifs/fire.gif",
  "media/gifs/cracks.gif",
  "media/gifs/fallingrock.gif",
  "media/gifs/landfalling.gif",
  "media/gifs/movingbuilding.gif",
  "media/gifs/tsunami.gif",
];

//LOAD THE PAGE
window.addEventListener("load", (event) => {
  $("#spinner").hide();
  $("#lomda").show();
});

function preload(arrayLoad) {
  $(arrayLoad).each(function () {
    $("<img/>")[0].src = this;
  });
}

//START FUNCTION
$(function () {
  setTimeout(() => {
    $("#startBtn").on("click", function () {
      $("#main").hide();
      // CHANGE TO PAGE1
      Page1();
      $("#page1").show().css("display", "flex");
      currentPage = 1;
      //
      $("#mainTitle").show().text(titles[currentPage - 1]);
      $("#nextBtn").css("display", "flex").on("click", pressedOnNext);
      $("#prevBtn").css("display", "flex").on("click", pressedOnPrevious);
    });
  }, 2500);
  preload(loadgifs);
  $("#popup").hide();
});

//PRESSING ON "NEXT" BUTTON
function pressedOnNext() {
  $("#nextBtn").off();
  $("#lomda").fadeOut(500);
  setTimeout(() => {
    changePage("next");
  }, 500);
  setTimeout(() => {
    $("#nextBtn").on("click", pressedOnNext);
  }, 1000);
}

//PRESSING ON "PREVIOUS" BUTTON
function pressedOnPrevious() {
  $("#prevBtn").off();
  $("#lomda").fadeOut(500);
  setTimeout(() => {
    changePage("prev");
  }, 500);
  setTimeout(() => {
    $("#prevBtn").on("click", pressedOnPrevious);
  }, 1000);
}

//CHANGE TO A DIFFERENT PAGE
function changePage(prevOrNext) {
  $("#page" + currentPage).hide();
  if (prevOrNext == "next") {
    currentPage++;
  } else if (prevOrNext == "prev") {
    currentPage--;
  }
  $("#page" + currentPage).show();
  $("#mainTitle").text(titles[currentPage - 1]);
  $("#lomda").fadeIn(500);
  eval("Page" + currentPage)();
}

// FUNCTIONS FOR EACH PAGE
function Page1() {
  $("#prevBtn").hide();
  $(".concept").on("click", function (e) {
    $(".concept").css("color", "black");
    $("#textConcept").text(basicConcepts[e.target.id.slice(-1) - 1])
    e.target.style.color = '#e6e1e1';
  });
}

function Page2() {
  $("#prevBtn").show();
  callPopup();
  $(".card").hover(
    function () {
      $(this)
      .children("img")
      .attr(
          "src",
          "media/gifs/" + $(this).children("img").attr("id") + ".gif"
          );
        },
        function () {
          $(this)
          .children("img")
        .attr("src", "media/" + $(this).children("img").attr("id") + ".jpg");
      }
      );
      //SHOW POPUP FUNCTION
      function callPopup() {
        $("#popup").show();
        $("#popup").animate({
          bottom: "5vh",
        },
        500,
        function () {
          $("#popup").animate({
            width: "40vw",
          },
          1000
          );
        }
        );
        setTimeout(function () {
          $("#popup").animate({
            width: "1vw",
            opacity: "0",
          },
          500,
          function () {
            $("#popup").animate({
              bottom: "-10vh",
            },
            1000
            );
          }
          );
        }, 5000);
      }
    }
    
    function Page3() {
      $("#popup").hide();
      $("#location").text("");
      $("#strength").text("");
      $("#deaths").text("");
      $(".circle").hover(
        function () {
          var numOfCircle = $(this).attr("id").slice(-1);
          $("#location").text(locations[numOfCircle]);
          $("#strength").text(strength[numOfCircle]);
          $("#deaths").text(deaths[numOfCircle]);
        },
        function () {
          $("#location").text("");
          $("#strength").text("");
          $("#deaths").text("");
        }
        );
      }
      
      function Page4() {
        
      }
      
      function Page5() {
        $("#nextArrow").off();
        $("#backArrow").off();
      }
      
      function Page6() {
        $("#titleResponse").text(responseTitles[currentResponse]);
        $("#textResponse").text(responseTexts[currentResponse]);
        $("#nextArrow").on("click", function () {
          currentResponse++;
          if (currentResponse === 5) {
            currentResponse = 0;
            $("#arrowImg").animate({
              top: "-=48vh",
            });
          } else {
            $("#arrowImg").animate({
              top: "+=12vh",
            });
          }
          pressedOnArrow();
        });
        $("#backArrow").on("click", function () {
          currentResponse--;
          if (currentResponse === -1) {
            currentResponse = 4;
            $("#arrowImg").animate({
              top: "+=48vh",
            });
          } else {
            $("#arrowImg").animate({
        top: "-=12vh",
      });
    }
    pressedOnArrow();
  });
  
  //PRESSING ON AN ARROW
  function pressedOnArrow() {
    if (currentResponse === 1) {
      $("#titleResponse").css("font-size", "2.6vw");
    } else {
      $("#titleResponse").css("font-size", "3vw");
    }
    $("#titleResponse").text(responseTitles[currentResponse]);
    $("#textResponse").text(responseTexts[currentResponse]);
  }
}

function Page7() {
  $("#nextArrow").off();
  $("#backArrow").off();
  $("#accordion").accordion({
    heightStyle: "content",
  });
}

function Page8() {
  index = undefined;
  animating = false;
  
  $(".shlav").css("filter", "brightness(100%)");
  $(".shlavContent").hide();
  $("#placeholder").delay(500).show();
  
  $(".shlav")
  .off()
  .click(function () {
    if (animating == false && index != $(".shlav").index(this)) {
      $(".shlav").eq(index).css("filter", "brightness(100%)");
      $(this).css("filter", "brightness(117%)");
      animating = true;
      $("#wrapper").animate({
        top: "100%",
      },
      400
      );
      setTimeout(() => {
        $("#placeholder").hide();
        $(".shlavContent").eq(index).hide();
        index = $(".shlav").index(this);
        $(".shlavContent").eq(index).show();
        $("#wrapper")
        .css({
          top: "-100%",
            })
            .animate({
              top: "0%",
            },
            500
            );
            setTimeout(() => {
              animating = false;
            }, 500);
          }, 500);
        }
      });
    }
    
    function Page9() {
      $("#page9").show().css("display", "flex");
      $("#nextBtn").text("המשך");
    }
    
    function Page10() {
  $("#page10").show().css("display", "flex");
  $("#nextBtn").text("לסיום");
  $(".responsibilityBox").click(function (e) {
    $("#preTextResponsibilities").fadeOut(500);
    setTimeout(() => {
      $(".responsibilitiesParts").fadeIn(500);
    }, 600);
    var clickedRes = e.target.id.slice(-1);
    $("#titleResponsibility").text(responsibilitiesTitles[clickedRes]);
    $("#textResponsibility").text(responsibilitiesTexts[clickedRes]);
    $("#imgResponsibility").attr("src", "media/responsibilities/" + "responsibility" + clickedRes + ".png");
  });
}

function Page11() {
  $("#page" + lastPage).css("display", "flex");
  $("#nextBtn").hide();
  $("#prevBtn").hide();
  $("#mainTitle").hide();
  $("#close").click(function () {
    $("#mainScreen").addClass("animate__animated animate__zoomOutDown");
    setTimeout(() => {
      window.close();
    }, 1000);
  });
}